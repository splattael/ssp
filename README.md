# Static Status Page

http://splattael-ssp.35.204.227.25.nip.io

Spike implementation to demonstrate how we could utilize GitLab features to create and deploy a static status page. See https://gitlab.com/gitlab-org/gitlab/issues/199876

## Workflow

1. User opens [issues](https://gitlab.com/splattael/ssp/issues) to describe an incident
2. User [triggers](https://gitlab.com/splattael/ssp/pipelines/new) a CI pipeline
3. CI job fetches issues via GitLab's Issues API and [generates markdown files](fetcher/run.rb)
4. CI job generates static pages using [Hugo](https://gohugo.io/)
5. CI job deploys this content via [GitLab's Auto DevOps](https://docs.gitlab.com/ee/topics/autodevops/) to a Kubernetes cluster (which runs nginx)

## Setup

1. Install Kubernetes cluster using GitLab's UI via **Operations > Kubernetes**
2. Install Helm tiller and Ingress
3. Add "Base domain" in the Kubernetes clusters view
5. Enable "Auto DevsOps" via **Settings > Auto DevOps**
5. Add the following CI variables via **Settings > Variables**
  - Create a personal access token and add as `GITLAB_API_PRIVATE_TOKEN` (Masked)
  - Set `AUTO_DEVOPS_BUILD_IMAGE_EXTRA_ARGS` to `--build-arg=CI_PROJECT_ID=$CI_PROJECT_ID --build-arg=GITLAB_API_PRIVATE_TOKEN=$GITLAB_API_PRIVATE_TOKEN --build-arg=GITLAB_API_ENDPOINT=$CI_API_V4_URL`
  - [Disable all jobs](https://docs.gitlab.com/ee/topics/autodevops/#disable-jobs) to speed-up Auto DevOps pipeline
