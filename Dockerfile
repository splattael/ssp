FROM ruby:2.7 AS fetcher

ARG CI_PROJECT_ID
# used by fetcher/run.rb
ARG GITLAB_API_PRIVATE_TOKEN
ARG GITLAB_API_ENDPOINT

WORKDIR /fetcher
COPY fetcher .
RUN mkdir ./posts
RUN bundle check || bundle install
RUN bundle exec ruby run.rb $CI_PROJECT_ID ./posts

FROM registry.gitlab.com/pages/hugo:latest AS static_site_generator
WORKDIR /content_generator
RUN mkdir -p /content_generator/content/posts
COPY content_generator .
COPY --from=fetcher /fetcher/posts/* /content_generator/content/posts/
RUN hugo --debug

FROM nginx:stable
COPY deployment/nginx.conf /etc/nginx/conf.d/default.conf
COPY --from=static_site_generator /content_generator/public /usr/share/nginx/html
