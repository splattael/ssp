require "gitlab"
require "fileutils"

ARGV.size or abort "$#0 content_directory/posts"
PROJECT_ID, CONTENT_DIR = *ARGV

POST_TEMPLATE = <<~MARKDOWN
---
title: %{title}
date: %{date}
draft: false
state: %{state}
---

%{description}
MARKDOWN

def undescore(text)
  text.downcase.gsub(/[^a-z0-9A-Z]/, '-')
end

def path_for(issue)
  "#{CONTENT_DIR}/#{issue.iid}.md"
end

def generate_content(issue)
  content = POST_TEMPLATE % {
    title: issue.title,
    description: issue.description,
    date: issue.created_at,
    state: issue.state
  }

  File.write(path_for(issue), content)
end

def each_issue(&block)
  Gitlab.issues(PROJECT_ID).auto_paginate(&block)
end

FileUtils.mkdir_p(CONTENT_DIR)

each_issue do |issue|
  generate_content(issue)
end
